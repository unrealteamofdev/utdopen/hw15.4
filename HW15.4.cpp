﻿#include <iostream>

const int N = 20; // Значение N

void PrintNumbers(int limit, bool printEven) {
    int start = printEven ? 0 : 1;
    int step = 2;

    for (int i = start; i <= limit; i += step) 
    {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    std::cout << "Чётные числа от 0 до " << N << ": ";
    PrintNumbers(N, true);

    std::cout << "Не чётные числа от 0 до " << N << ": ";
    PrintNumbers(N, false);

    return 0;
}
